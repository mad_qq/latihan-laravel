@extends('layout.master')

@section('judul')
Tambah data
@endsection

@section('content')

<h2>Tambah data</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama Casting</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan Umur dengan angka">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label><br>
                <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Bio"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>

@endsection