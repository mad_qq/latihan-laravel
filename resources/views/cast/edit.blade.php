@extends('layout.master')

@section('judul')
Edit Casting {{$cast->nama}}
@endsection

@section('content')

<h2>Edit data</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Casting</label>
                <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="Masukkan Umur dengan angka">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Bio</label><br>
                <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>

@endsection