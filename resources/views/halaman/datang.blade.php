@extends('layout.master')

@section('judul')
Welcome
@endsection

@section('content')
    <h1>Selamat datang! {{$first}}{{$last}}</h1>
    <h2>Terima kasih telah bergabung di website kami. Mari belajar bersama</h2>
    <p>{{$biodata}}</p>
    <p>Jenis Kelamin :
        @if ($gender == 1)
            Laki - Laki
        @else
            Perempuan
        @endif
    </p>
@endsection