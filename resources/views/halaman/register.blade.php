@extends('layout.master')

@section('judul')
Register
@endsection

@section('content')
    <form action="/kirim" method="post">
        @csrf
        <label>First Name : </label> <br><br>
        <input type="text" name="first"/> <br><br>

        <label>Last Name : </label> <br><br>
        <input type="text" name="last"/> <br><br>

        <label>Gender :</label> <br><br>
        <input type="radio" name="gender" value="1">Laki - laki <br>
        <input type="radio" name="gender" value="2">Perempuan <br><br>

        <label>Nationality :</label> <br><br>
        <select>
            <option>Indonesia</option>
            <option>Jepang</option>
            <option>Turki</option>
        </select> <br><br>

        <label>Language spoken : </label> <br><br>
        <input type="checkbox" name="bahasa" value="0">Indonesia <br>
        <input type="checkbox" name="bahasa" value="1">Inggris <br>
        <input type="checkbox" name="bahasa" value="2">Lainnya <br><br>

        <label>Biodata : </label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        
        <input type="submit" value="Sign Up"/>
    </form>
@endsection