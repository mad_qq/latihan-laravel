<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function kirim(Request $request)
    {
        $first = $request->first;
        $last = $request->last;
        $biodata = $request->bio;
        $gender = $request->gender;
        
        return view('halaman.datang', compact ('first','last','biodata', 'gender'));
    }
}
